package ru.studwork.sphere;

public class Sphere extends Circle {
    private double density;

    public Sphere(double radius, double density) {
        super(radius);
        this.density = density;
    }

    //Объем шара
    public double volume() {
        double volume = (4 * Math.PI * Math.pow(getCircleRadius(), 3)) / 3;
        return volume;
    }

    //Масса шара = density(Плотность) * volume(Объем)
    public double weight() {
        return density * this.volume();
    }
}
