package ru.studwork.sphere;
/**
 *  Создать иерархию классов Точка - Круг - Полный Шар.
 *  Класс Полный Шар должен содержать метод для вычисления
 *  веса шара в зависимости от удельного веса материала из
 *  которого сделан шар.Создать метод MAIN,в котором
 *  создается 2 полных шара(т.е 2 объекта класса Полный шар),
 *  определяется какой из шаров больше а какой тяжелее.
 */
public class Solution {
    public static void main(String[] args) {
        //Вторым параметром передается плотность материала
        Sphere sphereAl = new Sphere(90, 2.7);
        Sphere spherePl = new Sphere(70, 11.35); //Свинец по своей плотности больше, чем аллюминий

        System.out.println("Масса шара из Аллюминия: " + sphereAl.weight());
        System.out.println("Масса шара из Свинца: " + spherePl.weight());

        System.out.println("Объем шара из Аллюминия: " + sphereAl.volume());
        System.out.println("Объем шара из Свинца: " + spherePl.volume());

        //Сравниваем по объему
        if(sphereAl.volume() > spherePl.volume()) {
            System.out.println("Аллюминиевый шар больше");
        } else {
            System.out.println("Шар из свинца больше");
        }

        //Сравниваем по массе
        if(sphereAl.weight() > spherePl.weight()) {
            System.out.println("Шар из аллюминия тяжелее");
        } else {
            System.out.println("Шар из свинца тяжелее");
        }
    }
}
