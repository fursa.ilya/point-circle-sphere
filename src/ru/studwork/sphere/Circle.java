package ru.studwork.sphere;

//Класс Круг наследуется от Точки, имеет дополнительный параметр - радиус
public class Circle extends Point {
   private static final int DEFAULT_X = 5; //Координаты точки по умолчанию
   private static final int DEFAULT_Y = 5;
   private static final int DEFAULT_Z = 5;
   private double radius;

   public Circle(double radius) {
       super(DEFAULT_X, DEFAULT_Y, DEFAULT_Z);
       this.radius = radius;
   }

    public double getCircleRadius() {
        return radius;
    }

    public void setCircleRadius(double radius) {
        this.radius = radius;
    }

    public double circleSquare() { return (Math.PI * Math.pow(radius, 2)); }
}
